<?xml version="1.0" ?><!DOCTYPE TS><TS language="it_IT" sourcelanguage="en" version="2.1">
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/qtwrapper/callmanager_wrap.h" line="328"/>
        <source>Me</source>
        <translation>Io</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="61"/>
        <source>Hold</source>
        <translation>Pausa</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="63"/>
        <source>Talking</source>
        <translation>Parla</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="65"/>
        <source>ERROR</source>
        <translation>ERRORE</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="67"/>
        <source>Incoming</source>
        <translation>In arrivo</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="69"/>
        <source>Calling</source>
        <translation>Sta chiamando</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="71"/>
        <location filename="../src/chatview.cpp" line="61"/>
        <source>Connecting</source>
        <translation>Collegamento</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="73"/>
        <source>Searching</source>
        <translation>Ricerca</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="75"/>
        <source>Inactive</source>
        <translation>Non attivo</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="77"/>
        <location filename="../src/api/call.h" line="83"/>
        <location filename="../src/chatview.cpp" line="68"/>
        <source>Finished</source>
        <translation>Finito</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="79"/>
        <source>Timeout</source>
        <translation>Timeout</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="81"/>
        <source>Peer busy</source>
        <translation>Peer occupato</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="85"/>
        <source>Communication established</source>
        <translation>Comunicazione stabilita</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="130"/>
        <location filename="../src/authority/storagehelper.cpp" line="898"/>
        <source>Invitation received</source>
        <translation>Invito ricevuto</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="127"/>
        <location filename="../src/authority/storagehelper.cpp" line="896"/>
        <source>Contact added</source>
        <translation>Contatto aggiunto</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="104"/>
        <location filename="../src/authority/storagehelper.cpp" line="110"/>
        <location filename="../src/authority/storagehelper.cpp" line="894"/>
        <source>Outgoing call</source>
        <translation>Chiamata in uscita</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="106"/>
        <location filename="../src/authority/storagehelper.cpp" line="116"/>
        <source>Incoming call</source>
        <translation>Chiamata in arrivo</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="112"/>
        <location filename="../src/authority/storagehelper.cpp" line="892"/>
        <source>Missed outgoing call</source>
        <translation>Chiamata persa in uscita</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="118"/>
        <source>Missed incoming call</source>
        <translation>Chiamata persa in arrivo</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="132"/>
        <location filename="../src/authority/storagehelper.cpp" line="900"/>
        <source>Invitation accepted</source>
        <translation>Invito accettato</translation>
    </message>
    <message>
        <location filename="../src/avmodel.cpp" line="335"/>
        <location filename="../src/avmodel.cpp" line="354"/>
        <source>default</source>
        <translation>default</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="53"/>
        <source>Null</source>
        <translation>Null</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="54"/>
        <source>Trying</source>
        <translation>Tentativo</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="55"/>
        <source>Ringing</source>
        <translation>Squilla</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="56"/>
        <source>Being Forwarded</source>
        <translation>In inoltro</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="57"/>
        <source>Queued</source>
        <translation>In coda</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="58"/>
        <source>Progress</source>
        <translation>In corso</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="59"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="62"/>
        <location filename="../src/newcallmodel.cpp" line="60"/>
        <source>Accepted</source>
        <translation>Accettato</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="61"/>
        <source>Multiple Choices</source>
        <translation>Scelta multipla</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="62"/>
        <source>Moved Permanently</source>
        <translation>Spostato definitivamente</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="63"/>
        <source>Moved Temporarily</source>
        <translation>Spostato temporaneamente</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="64"/>
        <source>Use Proxy</source>
        <translation>Usa proxy</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="65"/>
        <source>Alternative Service</source>
        <translation>Servizio alternativo</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="66"/>
        <source>Bad Request</source>
        <translation>Richiesta errata</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="67"/>
        <source>Unauthorized</source>
        <translation>Non autorizzato</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="68"/>
        <source>Payment Required</source>
        <translation>Pagamento richiesto</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="69"/>
        <source>Forbidden</source>
        <translation>Vietato</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="70"/>
        <source>Not Found</source>
        <translation>Non trovato</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="71"/>
        <source>Method Not Allowed</source>
        <translation>Metodo non permesso</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="72"/>
        <location filename="../src/newcallmodel.cpp" line="92"/>
        <source>Not Acceptable</source>
        <translation>Non accettabile</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="73"/>
        <source>Proxy Authentication Required</source>
        <translation>Richiesta autenticazione proxy</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="74"/>
        <source>Request Timeout</source>
        <translation>Tempo scaduto</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="75"/>
        <source>Gone</source>
        <translation>Non disponible</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="76"/>
        <source>Request Entity Too Large</source>
        <translation>Entità richiesta troppo grande</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="77"/>
        <source>Request URI Too Long</source>
        <translation>URI richiesta troppo lungo</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="78"/>
        <source>Unsupported Media Type</source>
        <translation>Media Type non supportato</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="79"/>
        <source>Unsupported URI Scheme</source>
        <translation>Schema URI non supportato</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="80"/>
        <source>Bad Extension</source>
        <translation>Estensione errata</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="81"/>
        <source>Extension Required</source>
        <translation>Estensione obbligatoria</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="82"/>
        <source>Session Timer Too Small</source>
        <translation>Timer di sessione troppo piccolo</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="83"/>
        <source>Interval Too Brief</source>
        <translation>Intervallo troppo breve</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="84"/>
        <source>Temporarily Unavailable</source>
        <translation>Temporaneamente non disponibile</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="85"/>
        <source>Call TSX Does Not Exist</source>
        <translation>Chiamata TSX non esiste</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="86"/>
        <source>Loop Detected</source>
        <translation>Rilevato loop</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="87"/>
        <source>Too Many Hops</source>
        <translation>Troppi Hop</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="88"/>
        <source>Address Incomplete</source>
        <translation>Indirizzo incompleto</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="89"/>
        <source>Ambiguous</source>
        <translation>Ambiguo</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="90"/>
        <source>Busy</source>
        <translation>Occupato</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="91"/>
        <source>Request Terminated</source>
        <translation>Richiesta terminata</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="93"/>
        <source>Bad Event</source>
        <translation>Evento negativo</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="94"/>
        <source>Request Updated</source>
        <translation>Richiesta aggiornata</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="95"/>
        <source>Request Pending</source>
        <translation>Richiesta in corso</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="96"/>
        <source>Undecipherable</source>
        <translation>Indecifrabile</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="97"/>
        <source>Internal Server Error</source>
        <translation>Errore interno del server</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="98"/>
        <source>Not Implemented</source>
        <translation>Non implementato</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="99"/>
        <source>Bad Gateway</source>
        <translation>Gateway incorretto</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="100"/>
        <source>Service Unavailable</source>
        <translation>Servizio non disponibile</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="101"/>
        <source>Server Timeout</source>
        <translation>Timeout del server</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="102"/>
        <source>Version Not Supported</source>
        <translation>Versione non supportata</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="103"/>
        <source>Message Too Large</source>
        <translation>Messaggio troppo grande</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="104"/>
        <source>Precondition Failure</source>
        <translation>Fallimento della precondizione</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="105"/>
        <source>Busy Everywhere</source>
        <translation>Occupato ovunque</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="106"/>
        <source>Call Refused</source>
        <translation>Chiamata rifiutata</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="107"/>
        <source>Does Not Exist Anywhere</source>
        <translation>Non esiste da nessuna parte</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="108"/>
        <source>Not Acceptable Anywhere</source>
        <translation>Non accettabile ovunque</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="30"/>
        <source>Hide chat view</source>
        <translation>Nascondi la vista della chat</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="31"/>
        <source>Place video call</source>
        <translation>Effettua una videochiamata</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="32"/>
        <source>Place audio call</source>
        <translation>Metti la chiamata audio</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="33"/>
        <source>Add to conversations</source>
        <translation>Aggiungi alle conversazioni</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="34"/>
        <source>Unban contact</source>
        <translation>Contatto vietato</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="35"/>
        <source>Send</source>
        <translation>Invia</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="36"/>
        <source>Options</source>
        <translation>Opzioni</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="37"/>
        <source>Jump to latest</source>
        <translation>Vai all&apos;ultimo</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="38"/>
        <source>Send file</source>
        <translation>Invia File</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="39"/>
        <source>Leave video message</source>
        <translation>Lascia un videomessaggio</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="40"/>
        <source>Leave audio message</source>
        <translation>Lascia un messaggio audio</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="42"/>
        <source>Write to {0}</source>
        <translation>Scrivi a {0}</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="60"/>
        <source>Unable to make contact</source>
        <translation>Impossibile prendere contatto</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="65"/>
        <source>Waiting for contact</source>
        <translation>In attesa di contatto</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="66"/>
        <source>Incoming transfer</source>
        <translation>Trasferimento in entrata</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="67"/>
        <source>Timed out waiting for contact</source>
        <translation>Tempo scaduto in attesa di contatto</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="41"/>
        <source>Block</source>
        <translation>Blocca</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="44"/>
        <source>Note: an interaction will create a new contact.</source>
        <translation>Nota: un&apos;interazione creerà un nuovo contatto.</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="45"/>
        <source>is not in your contacts</source>
        <translation>non è nei tuoi contatti</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="47"/>
        <source>Note: you can automatically accept this invitation by sending a message.</source>
        <translation>Nota: puoi accettare automaticamente questo invito inviando un messaggio.</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="48"/>
        <source>{0} days ago</source>
        <translation>{0} giorni fa</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="49"/>
        <source>{0} hours ago</source>
        <translation>{0} ore fa</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="51"/>
        <source>{0} minutes ago</source>
        <translation>{0} minuti fa</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="63"/>
        <source>Canceled</source>
        <translation>Annullato</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="64"/>
        <source>Ongoing</source>
        <translation>In corso</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="48"/>
        <source>%d days ago</source>
        <translation>%d giorni fa</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="49"/>
        <source>%d hours ago</source>
        <translation>%d ore fa</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="51"/>
        <source>%d minutes ago</source>
        <translation>%d minuti fa</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="52"/>
        <source>one day ago</source>
        <translation>un giorno fa</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="53"/>
        <source>one hour ago</source>
        <translation>un&apos;ora fa</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="54"/>
        <source>just now</source>
        <translation>proprio adesso</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="55"/>
        <source>Failure</source>
        <translation>Fallito</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="56"/>
        <source>Confirm</source>
        <translation>Conferma</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="57"/>
        <source>Deny</source>
        <translation>Nega</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="58"/>
        <source>Delete</source>
        <translation>Elimina</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="59"/>
        <source>Retry</source>
        <translation>Riprova</translation>
    </message>
</context>
<context>
    <name>lrc::ContactModelPimpl</name>
    <message>
        <location filename="../src/contactmodel.cpp" line="455"/>
        <source>Searching…</source>
        <translation>Ricerca...</translation>
    </message>
    <message>
        <location filename="../src/contactmodel.cpp" line="950"/>
        <source>Invalid ID</source>
        <translation>ID non valido</translation>
    </message>
    <message>
        <location filename="../src/contactmodel.cpp" line="953"/>
        <source>Registered name not found</source>
        <translation>Nome registrato non trovato</translation>
    </message>
    <message>
        <location filename="../src/contactmodel.cpp" line="956"/>
        <source>Couldn&apos;t lookup…</source>
        <translation>Lookup non possibile…</translation>
    </message>
</context>
<context>
    <name>lrc::api::ContactModel</name>
    <message>
        <location filename="../src/contactmodel.cpp" line="419"/>
        <source>Bad URI scheme</source>
        <translation>Schema URI errato</translation>
    </message>
</context>
</TS>