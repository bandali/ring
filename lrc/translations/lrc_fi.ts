<?xml version="1.0" ?><!DOCTYPE TS><TS language="fi" sourcelanguage="en" version="2.1">
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/qtwrapper/callmanager_wrap.h" line="328"/>
        <source>Me</source>
        <translation>Minä</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="61"/>
        <source>Hold</source>
        <translation>Pidossa</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="63"/>
        <source>Talking</source>
        <translation>Puhuu</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="65"/>
        <source>ERROR</source>
        <translation>VIRHE</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="67"/>
        <source>Incoming</source>
        <translation>Saapuva</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="69"/>
        <source>Calling</source>
        <translation>Soittaa</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="71"/>
        <location filename="../src/chatview.cpp" line="61"/>
        <source>Connecting</source>
        <translation>Yhdistää</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="73"/>
        <source>Searching</source>
        <translation>Etsintä</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="75"/>
        <source>Inactive</source>
        <translation>Poissa</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="77"/>
        <location filename="../src/api/call.h" line="83"/>
        <location filename="../src/chatview.cpp" line="68"/>
        <source>Finished</source>
        <translation>Valmis</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="79"/>
        <source>Timeout</source>
        <translation>Aikakatkaisu</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="81"/>
        <source>Peer busy</source>
        <translation>Käyttäjä varattu</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="85"/>
        <source>Communication established</source>
        <translation>Yhteys muodostettu</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="130"/>
        <location filename="../src/authority/storagehelper.cpp" line="898"/>
        <source>Invitation received</source>
        <translation>Kutsu vastaanotettu</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="127"/>
        <location filename="../src/authority/storagehelper.cpp" line="896"/>
        <source>Contact added</source>
        <translation>Yhteystieto lisätty</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="104"/>
        <location filename="../src/authority/storagehelper.cpp" line="110"/>
        <location filename="../src/authority/storagehelper.cpp" line="894"/>
        <source>Outgoing call</source>
        <translation>Lähtevä puhelu</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="106"/>
        <location filename="../src/authority/storagehelper.cpp" line="116"/>
        <source>Incoming call</source>
        <translation>Saapuva puhelu</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="112"/>
        <location filename="../src/authority/storagehelper.cpp" line="892"/>
        <source>Missed outgoing call</source>
        <translation>Vastaamaton lähtevä puhelu</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="118"/>
        <source>Missed incoming call</source>
        <translation>Vastaamaton puhelu</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="132"/>
        <location filename="../src/authority/storagehelper.cpp" line="900"/>
        <source>Invitation accepted</source>
        <translation>Kutsu hyväksytty</translation>
    </message>
    <message>
        <location filename="../src/avmodel.cpp" line="335"/>
        <location filename="../src/avmodel.cpp" line="354"/>
        <source>default</source>
        <translation>oletus</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="53"/>
        <source>Null</source>
        <translation>Tyhjä</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="54"/>
        <source>Trying</source>
        <translation>Yritetään</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="55"/>
        <source>Ringing</source>
        <translation>Soi</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="56"/>
        <source>Being Forwarded</source>
        <translation>Välitetään eteenpäin</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="57"/>
        <source>Queued</source>
        <translation>Jonossa</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="58"/>
        <source>Progress</source>
        <translation>Edistyminen</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="59"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="62"/>
        <location filename="../src/newcallmodel.cpp" line="60"/>
        <source>Accepted</source>
        <translation>Hyväksytty</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="61"/>
        <source>Multiple Choices</source>
        <translation>Useita vaihtoehtoja</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="62"/>
        <source>Moved Permanently</source>
        <translation>Siirretty pysyvästi</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="63"/>
        <source>Moved Temporarily</source>
        <translation>Siirretty tilapäisesti</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="64"/>
        <source>Use Proxy</source>
        <translation>Käytä välityspalvelinta</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="65"/>
        <source>Alternative Service</source>
        <translation>Vaihtoehtoinen palvelu</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="66"/>
        <source>Bad Request</source>
        <translation>Virheellinen pyyntö</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="67"/>
        <source>Unauthorized</source>
        <translation>Luvaton</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="68"/>
        <source>Payment Required</source>
        <translation>Maksu vaaditaan</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="69"/>
        <source>Forbidden</source>
        <translation>Kielletty</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="70"/>
        <source>Not Found</source>
        <translation>Ei löydy</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="71"/>
        <source>Method Not Allowed</source>
        <translation>Menetelmää ei sallita</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="72"/>
        <location filename="../src/newcallmodel.cpp" line="92"/>
        <source>Not Acceptable</source>
        <translation>Ei hyväksyttävä</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="73"/>
        <source>Proxy Authentication Required</source>
        <translation>Välityspalvelimen todennus vaaditaan</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="74"/>
        <source>Request Timeout</source>
        <translation>Pyyntö aikakatkaistu</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="75"/>
        <source>Gone</source>
        <translation>Mennyt</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="76"/>
        <source>Request Entity Too Large</source>
        <translation>Pyyntö on liian suuri</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="77"/>
        <source>Request URI Too Long</source>
        <translation>Pyynnön verkko-osoite liian pitkä</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="78"/>
        <source>Unsupported Media Type</source>
        <translation>Ei tuettu mediatyyppi</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="79"/>
        <source>Unsupported URI Scheme</source>
        <translation>Verkko-osoitteen mallia ei tueta</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="80"/>
        <source>Bad Extension</source>
        <translation>Virheellinen laajennus</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="81"/>
        <source>Extension Required</source>
        <translation>Laajennus vaaditaan</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="82"/>
        <source>Session Timer Too Small</source>
        <translation>Istunnon ajastin on liian pieni</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="83"/>
        <source>Interval Too Brief</source>
        <translation>Väli liian lyhyt</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="84"/>
        <source>Temporarily Unavailable</source>
        <translation>Tilapäisesti poissa käytöstä</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="85"/>
        <source>Call TSX Does Not Exist</source>
        <translation>TSX-puhelua ei ole</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="86"/>
        <source>Loop Detected</source>
        <translation>Silmukka havaittu</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="87"/>
        <source>Too Many Hops</source>
        <translation>Liian monta hyppyä</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="88"/>
        <source>Address Incomplete</source>
        <translation>Osoite epätäydellinen</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="89"/>
        <source>Ambiguous</source>
        <translation>Epäselvä</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="90"/>
        <source>Busy</source>
        <translation>Varattu</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="91"/>
        <source>Request Terminated</source>
        <translation>Pyyntö lopetettu</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="93"/>
        <source>Bad Event</source>
        <translation>Huono tapahtuma</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="94"/>
        <source>Request Updated</source>
        <translation>Pyyntö päivitetty</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="95"/>
        <source>Request Pending</source>
        <translation>Pyyntö odottaa</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="96"/>
        <source>Undecipherable</source>
        <translation>Ei purettavissa</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="97"/>
        <source>Internal Server Error</source>
        <translation>Sisäinen palvelinvirhe</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="98"/>
        <source>Not Implemented</source>
        <translation>Ei toteutettu</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="99"/>
        <source>Bad Gateway</source>
        <translation>Huono yhdyskäytävä</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="100"/>
        <source>Service Unavailable</source>
        <translation>Palvelua ei käytettävissä</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="101"/>
        <source>Server Timeout</source>
        <translation>Palvelimen aikakatkaisu</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="102"/>
        <source>Version Not Supported</source>
        <translation>Versiota ei tueta</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="103"/>
        <source>Message Too Large</source>
        <translation>Viesti on liian suuri</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="104"/>
        <source>Precondition Failure</source>
        <translation>Ennakkoehdon virhe</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="105"/>
        <source>Busy Everywhere</source>
        <translation>Varattu kaikkialla</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="106"/>
        <source>Call Refused</source>
        <translation>Puhelu evätty</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="107"/>
        <source>Does Not Exist Anywhere</source>
        <translation>Ei ole missään</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="108"/>
        <source>Not Acceptable Anywhere</source>
        <translation>Ei hyväksytä missään</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="30"/>
        <source>Hide chat view</source>
        <translation>Piilota chat-näkymä</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="31"/>
        <source>Place video call</source>
        <translation>Aloita videopuhelu</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="32"/>
        <source>Place audio call</source>
        <translation>Aloita äänipuhelu</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="33"/>
        <source>Add to conversations</source>
        <translation>Lisää keskusteluihin</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="34"/>
        <source>Unban contact</source>
        <translation>Poista esto</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="35"/>
        <source>Send</source>
        <translation>Lähetä</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="36"/>
        <source>Options</source>
        <translation>Vaihtoehdot</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="37"/>
        <source>Jump to latest</source>
        <translation>Hyppää uusimpaan</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="38"/>
        <source>Send file</source>
        <translation>Lähetä tiedosto</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="39"/>
        <source>Leave video message</source>
        <translation>Jätä videoviesti</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="40"/>
        <source>Leave audio message</source>
        <translation>Jätä ääniviesti</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="42"/>
        <source>Write to {0}</source>
        <translation>Kirjoittaa {0}</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="60"/>
        <source>Unable to make contact</source>
        <translation>Yhteyden muodostaminen epäonnistui</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="65"/>
        <source>Waiting for contact</source>
        <translation>Odotetaan yhteyttä</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="66"/>
        <source>Incoming transfer</source>
        <translation>Saapuva siirto</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="67"/>
        <source>Timed out waiting for contact</source>
        <translation>Aikakatkaisu odottavalle yhteydelle</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="41"/>
        <source>Block</source>
        <translation>Estä</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="44"/>
        <source>Note: an interaction will create a new contact.</source>
        <translation>Huomaa: yhteydenotto tekee uuden yhteystiedon.</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="45"/>
        <source>is not in your contacts</source>
        <translation>ei ole yhteystiedoissasi</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="47"/>
        <source>Note: you can automatically accept this invitation by sending a message.</source>
        <translation>Huomaa: voit automaattisesti hyväksyä tämän kutsun lähettämällä viestin.</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="48"/>
        <source>{0} days ago</source>
        <translation>{0} päivää sitten</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="49"/>
        <source>{0} hours ago</source>
        <translation>{0} tuntia sitten</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="51"/>
        <source>{0} minutes ago</source>
        <translation>{0} minuuttia sitten</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="63"/>
        <source>Canceled</source>
        <translation>Peruttu</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="64"/>
        <source>Ongoing</source>
        <translation>Käynnissä</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="48"/>
        <source>%d days ago</source>
        <translation>%d päivää sitten</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="49"/>
        <source>%d hours ago</source>
        <translation>%d tuntia sitten</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="51"/>
        <source>%d minutes ago</source>
        <translation>%d minuuttia sitten</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="52"/>
        <source>one day ago</source>
        <translation>yksi päivä sitten</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="53"/>
        <source>one hour ago</source>
        <translation>yksi tunti sitten</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="54"/>
        <source>just now</source>
        <translation>juuri nyt</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="55"/>
        <source>Failure</source>
        <translation>Virhe</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="56"/>
        <source>Confirm</source>
        <translation>Vahvista</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="57"/>
        <source>Deny</source>
        <translation>Estä</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="58"/>
        <source>Delete</source>
        <translation>Poista</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="59"/>
        <source>Retry</source>
        <translation>Yritä uudelleen</translation>
    </message>
</context>
<context>
    <name>lrc::ContactModelPimpl</name>
    <message>
        <location filename="../src/contactmodel.cpp" line="455"/>
        <source>Searching…</source>
        <translation>Etsitään...</translation>
    </message>
    <message>
        <location filename="../src/contactmodel.cpp" line="950"/>
        <source>Invalid ID</source>
        <translation>Virheellinen tunnus</translation>
    </message>
    <message>
        <location filename="../src/contactmodel.cpp" line="953"/>
        <source>Registered name not found</source>
        <translation>Rekisteröityä nimeä ei löytynyt</translation>
    </message>
    <message>
        <location filename="../src/contactmodel.cpp" line="956"/>
        <source>Couldn&apos;t lookup…</source>
        <translation>Ei voitu etsiä...</translation>
    </message>
</context>
<context>
    <name>lrc::api::ContactModel</name>
    <message>
        <location filename="../src/contactmodel.cpp" line="419"/>
        <source>Bad URI scheme</source>
        <translation>Virheellinen verkko-osoitteen malli</translation>
    </message>
</context>
</TS>