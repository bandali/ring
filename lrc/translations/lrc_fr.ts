<?xml version="1.0" ?><!DOCTYPE TS><TS language="fr" sourcelanguage="en" version="2.1">
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/qtwrapper/callmanager_wrap.h" line="328"/>
        <source>Me</source>
        <translation>Moi</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="61"/>
        <source>Hold</source>
        <translation>Mettre en attente</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="63"/>
        <source>Talking</source>
        <translation>En cours</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="65"/>
        <source>ERROR</source>
        <translation>ERREUR</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="67"/>
        <source>Incoming</source>
        <translation>Appel entrant</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="69"/>
        <source>Calling</source>
        <translation>Appel en cours</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="71"/>
        <location filename="../src/chatview.cpp" line="61"/>
        <source>Connecting</source>
        <translation>En cours de connexion</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="73"/>
        <source>Searching</source>
        <translation>Recherche</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="75"/>
        <source>Inactive</source>
        <translation>Inactif</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="77"/>
        <location filename="../src/api/call.h" line="83"/>
        <location filename="../src/chatview.cpp" line="68"/>
        <source>Finished</source>
        <translation>Complété</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="79"/>
        <source>Timeout</source>
        <translation>Expiré</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="81"/>
        <source>Peer busy</source>
        <translation>Interlocuteur occupé</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="85"/>
        <source>Communication established</source>
        <translation>Communication établie</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="130"/>
        <location filename="../src/authority/storagehelper.cpp" line="898"/>
        <source>Invitation received</source>
        <translation>Invitation reçue</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="127"/>
        <location filename="../src/authority/storagehelper.cpp" line="896"/>
        <source>Contact added</source>
        <translation>Contact ajouté</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="104"/>
        <location filename="../src/authority/storagehelper.cpp" line="110"/>
        <location filename="../src/authority/storagehelper.cpp" line="894"/>
        <source>Outgoing call</source>
        <translation>Appel sortant</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="106"/>
        <location filename="../src/authority/storagehelper.cpp" line="116"/>
        <source>Incoming call</source>
        <translation>Appel entrant</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="112"/>
        <location filename="../src/authority/storagehelper.cpp" line="892"/>
        <source>Missed outgoing call</source>
        <translation>Appel sortant manqué</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="118"/>
        <source>Missed incoming call</source>
        <translation>Appel entrant manqué</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="132"/>
        <location filename="../src/authority/storagehelper.cpp" line="900"/>
        <source>Invitation accepted</source>
        <translation>Invitation acceptée</translation>
    </message>
    <message>
        <location filename="../src/avmodel.cpp" line="335"/>
        <location filename="../src/avmodel.cpp" line="354"/>
        <source>default</source>
        <translation>défaut</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="53"/>
        <source>Null</source>
        <translation>Null</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="54"/>
        <source>Trying</source>
        <translation>Essai en cours</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="55"/>
        <source>Ringing</source>
        <translation>Sonne</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="56"/>
        <source>Being Forwarded</source>
        <translation>En cours de transfert</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="57"/>
        <source>Queued</source>
        <translation>Mis dans la file d&apos;attente</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="58"/>
        <source>Progress</source>
        <translation>En cours</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="59"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="62"/>
        <location filename="../src/newcallmodel.cpp" line="60"/>
        <source>Accepted</source>
        <translation>Accepté</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="61"/>
        <source>Multiple Choices</source>
        <translation>Choix multiples</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="62"/>
        <source>Moved Permanently</source>
        <translation>Définitivement déplacé</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="63"/>
        <source>Moved Temporarily</source>
        <translation>Temporairement déplacé</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="64"/>
        <source>Use Proxy</source>
        <translation>Utiliser un serveur délégataire</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="65"/>
        <source>Alternative Service</source>
        <translation>Service alternatif</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="66"/>
        <source>Bad Request</source>
        <translation>Requête invalide</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="67"/>
        <source>Unauthorized</source>
        <translation>Non authorisé</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="68"/>
        <source>Payment Required</source>
        <translation>Payement nécessaire</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="69"/>
        <source>Forbidden</source>
        <translation>Interdit</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="70"/>
        <source>Not Found</source>
        <translation>Introuvable</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="71"/>
        <source>Method Not Allowed</source>
        <translation>Méthode non permise</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="72"/>
        <location filename="../src/newcallmodel.cpp" line="92"/>
        <source>Not Acceptable</source>
        <translation>N&apos;est pas acceptable</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="73"/>
        <source>Proxy Authentication Required</source>
        <translation>Authentification au serveur mandataire nécessaire</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="74"/>
        <source>Request Timeout</source>
        <translation>Requête expirée</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="75"/>
        <source>Gone</source>
        <translation>Parti</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="76"/>
        <source>Request Entity Too Large</source>
        <translation>L&apos;élément demandé est trop gros</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="77"/>
        <source>Request URI Too Long</source>
        <translation>l&apos;URI demandée est trop longue</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="78"/>
        <source>Unsupported Media Type</source>
        <translation>Type de média non supporté</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="79"/>
        <source>Unsupported URI Scheme</source>
        <translation>Schéma d&apos;URI non supporté</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="80"/>
        <source>Bad Extension</source>
        <translation>Mauvaise extension</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="81"/>
        <source>Extension Required</source>
        <translation>Extension nécessaire</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="82"/>
        <source>Session Timer Too Small</source>
        <translation>Compteur de session trop petit</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="83"/>
        <source>Interval Too Brief</source>
        <translation>Intervalle trop court</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="84"/>
        <source>Temporarily Unavailable</source>
        <translation>Temporairement non disponible</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="85"/>
        <source>Call TSX Does Not Exist</source>
        <translation>L&apos;appel TSX n’existe pas</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="86"/>
        <source>Loop Detected</source>
        <translation>Boucle détectée</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="87"/>
        <source>Too Many Hops</source>
        <translation>Trop de sauts</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="88"/>
        <source>Address Incomplete</source>
        <translation>Adresse incomplète</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="89"/>
        <source>Ambiguous</source>
        <translation>Ambigu</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="90"/>
        <source>Busy</source>
        <translation>Occupé</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="91"/>
        <source>Request Terminated</source>
        <translation>La requête est terminée</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="93"/>
        <source>Bad Event</source>
        <translation>Événement incorrect</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="94"/>
        <source>Request Updated</source>
        <translation>Requête modifiée</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="95"/>
        <source>Request Pending</source>
        <translation>Requête en attente</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="96"/>
        <source>Undecipherable</source>
        <translation>Indéchiffrable</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="97"/>
        <source>Internal Server Error</source>
        <translation>Erreur Interne du Serveur</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="98"/>
        <source>Not Implemented</source>
        <translation>Non Implémenté</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="99"/>
        <source>Bad Gateway</source>
        <translation>Mauvaise passerelle</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="100"/>
        <source>Service Unavailable</source>
        <translation>Service indisponible</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="101"/>
        <source>Server Timeout</source>
        <translation>Dépassement de délai du serveur</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="102"/>
        <source>Version Not Supported</source>
        <translation>Version non supportée</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="103"/>
        <source>Message Too Large</source>
        <translation>Message Trop Large</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="104"/>
        <source>Precondition Failure</source>
        <translation>Échec de Précondition </translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="105"/>
        <source>Busy Everywhere</source>
        <translation>Occupé partout</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="106"/>
        <source>Call Refused</source>
        <translation>Appel refusé</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="107"/>
        <source>Does Not Exist Anywhere</source>
        <translation>N&apos;existe nul part</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="108"/>
        <source>Not Acceptable Anywhere</source>
        <translation>N&apos;est pas acceptable ici</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="30"/>
        <source>Hide chat view</source>
        <translation>Cacher la fenêtre de clavardage</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="31"/>
        <source>Place video call</source>
        <translation>Faire un appel vidéo</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="32"/>
        <source>Place audio call</source>
        <translation>Faire un appel audio</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="33"/>
        <source>Add to conversations</source>
        <translation>Ajouter aux conversations</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="34"/>
        <source>Unban contact</source>
        <translation>Débloquer le contact</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="35"/>
        <source>Send</source>
        <translation>Envoyer</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="36"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="37"/>
        <source>Jump to latest</source>
        <translation>Allez à la fin</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="38"/>
        <source>Send file</source>
        <translation>Envoyez un fichier</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="39"/>
        <source>Leave video message</source>
        <translation>Envoyer un message vidéo</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="40"/>
        <source>Leave audio message</source>
        <translation>Envoyer un message vocal</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="42"/>
        <source>Write to {0}</source>
        <translation>Écrire à {0}</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="60"/>
        <source>Unable to make contact</source>
        <translation>Impossible de contacter l&apos;interlocuteur</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="65"/>
        <source>Waiting for contact</source>
        <translation>En attente de votre contact</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="66"/>
        <source>Incoming transfer</source>
        <translation>Transfert entrant</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="67"/>
        <source>Timed out waiting for contact</source>
        <translation>Délai d&apos;attente de votre contact dépassé</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="41"/>
        <source>Block</source>
        <translation>Bloquer</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="44"/>
        <source>Note: an interaction will create a new contact.</source>
        <translation>Remarque: une interaction créera un nouveau contact.</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="45"/>
        <source>is not in your contacts</source>
        <translation>ne fait pas parti de vos contacts</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="47"/>
        <source>Note: you can automatically accept this invitation by sending a message.</source>
        <translation>Remarque: vous pouvez automatiquement accepter une invitation en lui répondant.</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="48"/>
        <source>{0} days ago</source>
        <translation>Il y a {0} jours</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="49"/>
        <source>{0} hours ago</source>
        <translation>il y a {0} heures</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="51"/>
        <source>{0} minutes ago</source>
        <translation>il y a {0} minutes</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="63"/>
        <source>Canceled</source>
        <translation>Annulé</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="64"/>
        <source>Ongoing</source>
        <translation>En cours</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="48"/>
        <source>%d days ago</source>
        <translation>Il y a %d jours</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="49"/>
        <source>%d hours ago</source>
        <translation>Il y a  %d heures</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="51"/>
        <source>%d minutes ago</source>
        <translation>Il y a %d  minutes</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="52"/>
        <source>one day ago</source>
        <translation>il y a une journée</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="53"/>
        <source>one hour ago</source>
        <translation>il y a une heure</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="54"/>
        <source>just now</source>
        <translation>juste maintenant</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="55"/>
        <source>Failure</source>
        <translation>Échec</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="56"/>
        <source>Confirm</source>
        <translation>Confirmer</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="57"/>
        <source>Deny</source>
        <translation>Refuser</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="58"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="59"/>
        <source>Retry</source>
        <translation>Réessaie</translation>
    </message>
</context>
<context>
    <name>lrc::ContactModelPimpl</name>
    <message>
        <location filename="../src/contactmodel.cpp" line="455"/>
        <source>Searching…</source>
        <translation>Recherche en cours ...</translation>
    </message>
    <message>
        <location filename="../src/contactmodel.cpp" line="950"/>
        <source>Invalid ID</source>
        <translation>ID invalide</translation>
    </message>
    <message>
        <location filename="../src/contactmodel.cpp" line="953"/>
        <source>Registered name not found</source>
        <translation>Nom d&apos;enregistrement non trouvé</translation>
    </message>
    <message>
        <location filename="../src/contactmodel.cpp" line="956"/>
        <source>Couldn&apos;t lookup…</source>
        <translation>Impossible de vérifier le nom d&apos;utilisateur</translation>
    </message>
</context>
<context>
    <name>lrc::api::ContactModel</name>
    <message>
        <location filename="../src/contactmodel.cpp" line="419"/>
        <source>Bad URI scheme</source>
        <translation>Schéma d&apos;URI non supporté</translation>
    </message>
</context>
</TS>