<?xml version="1.0" ?><!DOCTYPE TS><TS language="sq_AL" sourcelanguage="en" version="2.1">
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/qtwrapper/callmanager_wrap.h" line="328"/>
        <source>Me</source>
        <translation>Unë</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="61"/>
        <source>Hold</source>
        <translation>Mbaje</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="63"/>
        <source>Talking</source>
        <translation>Po bisedohet</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="65"/>
        <source>ERROR</source>
        <translation>GABIM</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="67"/>
        <source>Incoming</source>
        <translation>Ardhëse</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="69"/>
        <source>Calling</source>
        <translation>Po thirret</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="71"/>
        <location filename="../src/chatview.cpp" line="61"/>
        <source>Connecting</source>
        <translation>Po lidhet</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="73"/>
        <source>Searching</source>
        <translation>Po kërkohet</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="75"/>
        <source>Inactive</source>
        <translation>Jo aktiv</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="77"/>
        <location filename="../src/api/call.h" line="83"/>
        <location filename="../src/chatview.cpp" line="68"/>
        <source>Finished</source>
        <translation>Përfundoi</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="79"/>
        <source>Timeout</source>
        <translation>Mbarim Kohe</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="81"/>
        <source>Peer busy</source>
        <translation>Ana tjetër e zënë</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="85"/>
        <source>Communication established</source>
        <translation>Komunikimi u vendos</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="130"/>
        <location filename="../src/authority/storagehelper.cpp" line="898"/>
        <source>Invitation received</source>
        <translation>Ftesa u mor</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="127"/>
        <location filename="../src/authority/storagehelper.cpp" line="896"/>
        <source>Contact added</source>
        <translation>Kontakti u shtua</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="104"/>
        <location filename="../src/authority/storagehelper.cpp" line="110"/>
        <location filename="../src/authority/storagehelper.cpp" line="894"/>
        <source>Outgoing call</source>
        <translation>Thirrje për</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="106"/>
        <location filename="../src/authority/storagehelper.cpp" line="116"/>
        <source>Incoming call</source>
        <translation>Thirrje ardhëse</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="112"/>
        <location filename="../src/authority/storagehelper.cpp" line="892"/>
        <source>Missed outgoing call</source>
        <translation>Thirrje ikse të humbura</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="118"/>
        <source>Missed incoming call</source>
        <translation>Thirrje ardhëse të humbura</translation>
    </message>
    <message>
        <location filename="../src/authority/storagehelper.cpp" line="132"/>
        <location filename="../src/authority/storagehelper.cpp" line="900"/>
        <source>Invitation accepted</source>
        <translation>Ftesa u pranua</translation>
    </message>
    <message>
        <location filename="../src/avmodel.cpp" line="335"/>
        <location filename="../src/avmodel.cpp" line="354"/>
        <source>default</source>
        <translation>parazgjedhje</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="53"/>
        <source>Null</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="54"/>
        <source>Trying</source>
        <translation>Po provohet</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="55"/>
        <source>Ringing</source>
        <translation>Po i bihet ziles</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="56"/>
        <source>Being Forwarded</source>
        <translation>Po Përcillet</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="57"/>
        <source>Queued</source>
        <translation>Në Radhë</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="58"/>
        <source>Progress</source>
        <translation>Ecuri</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="59"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="62"/>
        <location filename="../src/newcallmodel.cpp" line="60"/>
        <source>Accepted</source>
        <translation>E pranuar</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="61"/>
        <source>Multiple Choices</source>
        <translation>Zgjedhje të Shumta</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="62"/>
        <source>Moved Permanently</source>
        <translation>Lëvizur Përgjithmonë</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="63"/>
        <source>Moved Temporarily</source>
        <translation>Lëvizur Përkohësisht</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="64"/>
        <source>Use Proxy</source>
        <translation>Përdor Ndërmjetës</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="65"/>
        <source>Alternative Service</source>
        <translation>Shërbim Alternativ</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="66"/>
        <source>Bad Request</source>
        <translation>Kërkesë e Gabuar</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="67"/>
        <source>Unauthorized</source>
        <translation>E paautorizuar</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="68"/>
        <source>Payment Required</source>
        <translation>Lypset Pagesë</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="69"/>
        <source>Forbidden</source>
        <translation>E ndaluar</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="70"/>
        <source>Not Found</source>
        <translation>S’u Gjet</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="71"/>
        <source>Method Not Allowed</source>
        <translation>Metodë e Palejuar</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="72"/>
        <location filename="../src/newcallmodel.cpp" line="92"/>
        <source>Not Acceptable</source>
        <translation>E papranueshme</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="73"/>
        <source>Proxy Authentication Required</source>
        <translation>Lypset Mirëfilltësim Ndërmjetësi</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="74"/>
        <source>Request Timeout</source>
        <translation>Mbarim Kohe për Kërkesën</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="75"/>
        <source>Gone</source>
        <translation>U avullua</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="76"/>
        <source>Request Entity Too Large</source>
        <translation>Njësi Kërkese Shumë e Gjatë</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="77"/>
        <source>Request URI Too Long</source>
        <translation>URI Kërkese Shumë i Gjatë</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="78"/>
        <source>Unsupported Media Type</source>
        <translation>Lloj Media i Pambuluar</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="79"/>
        <source>Unsupported URI Scheme</source>
        <translation>Skemë URI e Pambuluar</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="80"/>
        <source>Bad Extension</source>
        <translation>Zgjatim i Gabuar</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="81"/>
        <source>Extension Required</source>
        <translation>Zgjatim i Domosdoshëm</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="82"/>
        <source>Session Timer Too Small</source>
        <translation>Kohëmatës Sesioni Shumë i Vogël</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="83"/>
        <source>Interval Too Brief</source>
        <translation>Interval Shumë i Shkurtër</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="84"/>
        <source>Temporarily Unavailable</source>
        <translation>Përkohësisht Jo i Passhëm</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="85"/>
        <source>Call TSX Does Not Exist</source>
        <translation>S’ekziston TSX Thirrjeje</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="86"/>
        <source>Loop Detected</source>
        <translation>U pikas Qerthull</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="87"/>
        <source>Too Many Hops</source>
        <translation>Shumë Kërcime</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="88"/>
        <source>Address Incomplete</source>
        <translation>Adresë e Paplotë</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="89"/>
        <source>Ambiguous</source>
        <translation>E dykuptimtë</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="90"/>
        <source>Busy</source>
        <translation>I zënë</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="91"/>
        <source>Request Terminated</source>
        <translation>Kërkesa u Përfundua</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="93"/>
        <source>Bad Event</source>
        <translation>Akt i Gabuar</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="94"/>
        <source>Request Updated</source>
        <translation>Kërkesa u Përditësua</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="95"/>
        <source>Request Pending</source>
        <translation>Kërkesë Pezull</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="96"/>
        <source>Undecipherable</source>
        <translation>E padeshifrueshme</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="97"/>
        <source>Internal Server Error</source>
        <translation>Gabim i Brendshëm Shërbyesi</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="98"/>
        <source>Not Implemented</source>
        <translation>E pasendërtuar</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="99"/>
        <source>Bad Gateway</source>
        <translation>Kanal i Gabuar</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="100"/>
        <source>Service Unavailable</source>
        <translation>Shërbim i Pakapshëm</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="101"/>
        <source>Server Timeout</source>
        <translation>Mbarim Kohe Shërbyesi</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="102"/>
        <source>Version Not Supported</source>
        <translation>Version i Pambuluar</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="103"/>
        <source>Message Too Large</source>
        <translation>Mesazh Shumë i Gjatë</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="104"/>
        <source>Precondition Failure</source>
        <translation>Dështim Parakushti</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="105"/>
        <source>Busy Everywhere</source>
        <translation>Ngado i Zënë</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="106"/>
        <source>Call Refused</source>
        <translation>Thirrje e Hedhur Poshtë</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="107"/>
        <source>Does Not Exist Anywhere</source>
        <translation>S’ekziston Gjëkundi</translation>
    </message>
    <message>
        <location filename="../src/newcallmodel.cpp" line="108"/>
        <source>Not Acceptable Anywhere</source>
        <translation>E papranuar Gjëkundi</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="30"/>
        <source>Hide chat view</source>
        <translation>Fshihe pamjen fjalosje</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="31"/>
        <source>Place video call</source>
        <translation>Bëni një thirrje video</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="32"/>
        <source>Place audio call</source>
        <translation>Bëni një thirrje audio</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="33"/>
        <source>Add to conversations</source>
        <translation>Shtoje te biseda</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="34"/>
        <source>Unban contact</source>
        <translation>Hiqja dëbimin kontaktit</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="35"/>
        <source>Send</source>
        <translation>Dërgoje</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="36"/>
        <source>Options</source>
        <translation>Mundësi</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="37"/>
        <source>Jump to latest</source>
        <translation>Hidhu te më i riu</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="38"/>
        <source>Send file</source>
        <translation>Dërgo kartelë</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="39"/>
        <source>Leave video message</source>
        <translation>Lini mesazh video</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="40"/>
        <source>Leave audio message</source>
        <translation>Lini mesazh audio</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="42"/>
        <source>Write to {0}</source>
        <translation>Shkruaje te {0}</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="60"/>
        <source>Unable to make contact</source>
        <translation>S’arrihet të vendoset kontakt</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="65"/>
        <source>Waiting for contact</source>
        <translation>Po pritet për kontakt</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="66"/>
        <source>Incoming transfer</source>
        <translation>Shpërngulje ardhëse</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="67"/>
        <source>Timed out waiting for contact</source>
        <translation>Mbaroi koha duke pritur për kontakt</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="41"/>
        <source>Block</source>
        <translation>Bllokoje</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="44"/>
        <source>Note: an interaction will create a new contact.</source>
        <translation>Shënim: një ndërveprim do të krijojë një kontakt të ri.</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="45"/>
        <source>is not in your contacts</source>
        <translation>s’gjendet në kontaktet tuaja</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="47"/>
        <source>Note: you can automatically accept this invitation by sending a message.</source>
        <translation>Shënim: mund ta pranoni automatikisht këtë ftesë duke dërguar një mesazh.</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="48"/>
        <source>{0} days ago</source>
        <translation>{0} ditë më parë</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="49"/>
        <source>{0} hours ago</source>
        <translation>{0} orë më parë</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="51"/>
        <source>{0} minutes ago</source>
        <translation>{0} minuta më parë</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="63"/>
        <source>Canceled</source>
        <translation>U anulua</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="64"/>
        <source>Ongoing</source>
        <translation>Në punë e sipër</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="48"/>
        <source>%d days ago</source>
        <translation>%d ditë më parë</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="49"/>
        <source>%d hours ago</source>
        <translation>%d orë më parë </translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="51"/>
        <source>%d minutes ago</source>
        <translation>%d minuta më parë</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="52"/>
        <source>one day ago</source>
        <translation>një ditë më parë</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="53"/>
        <source>one hour ago</source>
        <translation>një orë më parë</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="54"/>
        <source>just now</source>
        <translation>mu tani</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="55"/>
        <source>Failure</source>
        <translation>Dështim</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="56"/>
        <source>Confirm</source>
        <translation>Ripohoje</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="57"/>
        <source>Deny</source>
        <translation>Mohoje</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="58"/>
        <source>Delete</source>
        <translation>Fshije</translation>
    </message>
    <message>
        <location filename="../src/chatview.cpp" line="59"/>
        <source>Retry</source>
        <translation>Riprovoni</translation>
    </message>
</context>
<context>
    <name>lrc::ContactModelPimpl</name>
    <message>
        <location filename="../src/contactmodel.cpp" line="455"/>
        <source>Searching…</source>
        <translation>Po kërkohet…</translation>
    </message>
    <message>
        <location filename="../src/contactmodel.cpp" line="950"/>
        <source>Invalid ID</source>
        <translation>ID e pavlefshme</translation>
    </message>
    <message>
        <location filename="../src/contactmodel.cpp" line="953"/>
        <source>Registered name not found</source>
        <translation>S’u gjet emër i regjistruar</translation>
    </message>
    <message>
        <location filename="../src/contactmodel.cpp" line="956"/>
        <source>Couldn&apos;t lookup…</source>
        <translation>S’u kërkua dot…</translation>
    </message>
</context>
<context>
    <name>lrc::api::ContactModel</name>
    <message>
        <location filename="../src/contactmodel.cpp" line="419"/>
        <source>Bad URI scheme</source>
        <translation>Skemë URI i gabuar</translation>
    </message>
</context>
</TS>